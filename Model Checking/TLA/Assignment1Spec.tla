-------------------------- MODULE Assignment1Spec --------------------------
EXTENDS TLC, Integers, Sequences
CONSTANTS FileNumber, PagesPerFile, WordsPerPage

(*--algorithm message_queue
variables fileElaborated = 0, mainEnd = FALSE, FileQueue = <<>>, PageQueue = <<>>, WordQueue = <<>>;
 
define
    AllWordLoad == [](<>(Len(WordQueue) = WordsPerPage*PagesPerFile*FileNumber))
    BuondedQueue == Len(PageQueue) <= PagesPerFile*FileNumber /\ Len(WordQueue) <= WordsPerPage*PagesPerFile*FileNumber
end define;

macro putNTimes(queue, elem, n) begin
    while n > 0 do
        queue := Append(queue, elem);
        n := n - 1;
    end while;
end macro;

fair process main = "Main"
variables file = "File";
begin main:
    while Len(FileQueue) < FileNumber do
        FileQueue := Append(FileQueue, file);
    end while;
    mainEnd:=TRUE;
end process;

fair process loader \in {"Loader1", "Loader2"}
variables item = "", page = "Page", i = PagesPerFile;
begin Load:
    await mainEnd;
    while_loader:
    while FileQueue /= <<>> do
        item := Head(FileQueue);
        FileQueue := Tail(FileQueue);
        load_loader:
            print "Load Page";
        put_loader: 
            putNTimes(PageQueue, page, i);
            i := PagesPerFile; 
            fileElaborated := fileElaborated + 1;
    end while;
end process;

fair process parser \in {"Parser1", "Parser2"}
variable item = "", word = "Word", i = WordsPerPage;
begin Parser:
    await fileElaborated /= 0;
    while_loader:
    while fileElaborated < FileNumber \/ PageQueue /= <<>> do
            await PageQueue /= <<>>;
            item := Head(PageQueue);
            PageQueue := Tail(PageQueue);
        parse_parser:
            print "Parse Page";
        put_parser:
            putNTimes(WordQueue, word, i);
            i := WordsPerPage;
    end while;
end process;
end algorithm;*)
\* BEGIN TRANSLATION (chksum(pcal) = "1d3945d5" /\ chksum(tla) = "a323b6ad")
\* Label main of process main at line 23 col 5 changed to main_
\* Label while_loader of process loader at line 34 col 5 changed to while_loader_
\* Process variable item of process loader at line 30 col 11 changed to item_
\* Process variable i of process loader at line 30 col 37 changed to i_
VARIABLES fileElaborated, mainEnd, FileQueue, PageQueue, WordQueue, pc

(* define statement *)
AllWordLoad == [](<>(Len(WordQueue) = WordsPerPage*PagesPerFile*FileNumber))
BuondedQueue == Len(PageQueue) <= PagesPerFile*FileNumber /\ Len(WordQueue) <= WordsPerPage*PagesPerFile*FileNumber

VARIABLES file, item_, page, i_, item, word, i

vars == << fileElaborated, mainEnd, FileQueue, PageQueue, WordQueue, pc, file, 
           item_, page, i_, item, word, i >>

ProcSet == {"Main"} \cup ({"Loader1", "Loader2"}) \cup ({"Parser1", "Parser2"})

Init == (* Global variables *)
        /\ fileElaborated = 0
        /\ mainEnd = FALSE
        /\ FileQueue = <<>>
        /\ PageQueue = <<>>
        /\ WordQueue = <<>>
        (* Process main *)
        /\ file = "File"
        (* Process loader *)
        /\ item_ = [self \in {"Loader1", "Loader2"} |-> ""]
        /\ page = [self \in {"Loader1", "Loader2"} |-> "Page"]
        /\ i_ = [self \in {"Loader1", "Loader2"} |-> PagesPerFile]
        (* Process parser *)
        /\ item = [self \in {"Parser1", "Parser2"} |-> ""]
        /\ word = [self \in {"Parser1", "Parser2"} |-> "Word"]
        /\ i = [self \in {"Parser1", "Parser2"} |-> WordsPerPage]
        /\ pc = [self \in ProcSet |-> CASE self = "Main" -> "main_"
                                        [] self \in {"Loader1", "Loader2"} -> "Load"
                                        [] self \in {"Parser1", "Parser2"} -> "Parser"]

main_ == /\ pc["Main"] = "main_"
         /\ IF Len(FileQueue) < FileNumber
               THEN /\ FileQueue' = Append(FileQueue, file)
                    /\ pc' = [pc EXCEPT !["Main"] = "main_"]
                    /\ UNCHANGED mainEnd
               ELSE /\ mainEnd' = TRUE
                    /\ pc' = [pc EXCEPT !["Main"] = "Done"]
                    /\ UNCHANGED FileQueue
         /\ UNCHANGED << fileElaborated, PageQueue, WordQueue, file, item_, 
                         page, i_, item, word, i >>

main == main_

Load(self) == /\ pc[self] = "Load"
              /\ mainEnd
              /\ pc' = [pc EXCEPT ![self] = "while_loader_"]
              /\ UNCHANGED << fileElaborated, mainEnd, FileQueue, PageQueue, 
                              WordQueue, file, item_, page, i_, item, word, i >>

while_loader_(self) == /\ pc[self] = "while_loader_"
                       /\ IF FileQueue /= <<>>
                             THEN /\ item_' = [item_ EXCEPT ![self] = Head(FileQueue)]
                                  /\ FileQueue' = Tail(FileQueue)
                                  /\ pc' = [pc EXCEPT ![self] = "load_loader"]
                             ELSE /\ pc' = [pc EXCEPT ![self] = "Done"]
                                  /\ UNCHANGED << FileQueue, item_ >>
                       /\ UNCHANGED << fileElaborated, mainEnd, PageQueue, 
                                       WordQueue, file, page, i_, item, word, 
                                       i >>

load_loader(self) == /\ pc[self] = "load_loader"
                     /\ PrintT("Load Page")
                     /\ pc' = [pc EXCEPT ![self] = "put_loader"]
                     /\ UNCHANGED << fileElaborated, mainEnd, FileQueue, 
                                     PageQueue, WordQueue, file, item_, page, 
                                     i_, item, word, i >>

put_loader(self) == /\ pc[self] = "put_loader"
                    /\ IF i_[self] > 0
                          THEN /\ PageQueue' = Append(PageQueue, page[self])
                               /\ i_' = [i_ EXCEPT ![self] = i_[self] - 1]
                               /\ pc' = [pc EXCEPT ![self] = "put_loader"]
                               /\ UNCHANGED fileElaborated
                          ELSE /\ i_' = [i_ EXCEPT ![self] = PagesPerFile]
                               /\ fileElaborated' = fileElaborated + 1
                               /\ pc' = [pc EXCEPT ![self] = "while_loader_"]
                               /\ UNCHANGED PageQueue
                    /\ UNCHANGED << mainEnd, FileQueue, WordQueue, file, item_, 
                                    page, item, word, i >>

loader(self) == Load(self) \/ while_loader_(self) \/ load_loader(self)
                   \/ put_loader(self)

Parser(self) == /\ pc[self] = "Parser"
                /\ fileElaborated /= 0
                /\ pc' = [pc EXCEPT ![self] = "while_loader"]
                /\ UNCHANGED << fileElaborated, mainEnd, FileQueue, PageQueue, 
                                WordQueue, file, item_, page, i_, item, word, 
                                i >>

while_loader(self) == /\ pc[self] = "while_loader"
                      /\ IF fileElaborated < FileNumber \/ PageQueue /= <<>>
                            THEN /\ PageQueue /= <<>>
                                 /\ item' = [item EXCEPT ![self] = Head(PageQueue)]
                                 /\ PageQueue' = Tail(PageQueue)
                                 /\ pc' = [pc EXCEPT ![self] = "parse_parser"]
                            ELSE /\ pc' = [pc EXCEPT ![self] = "Done"]
                                 /\ UNCHANGED << PageQueue, item >>
                      /\ UNCHANGED << fileElaborated, mainEnd, FileQueue, 
                                      WordQueue, file, item_, page, i_, word, 
                                      i >>

parse_parser(self) == /\ pc[self] = "parse_parser"
                      /\ PrintT("Parse Page")
                      /\ pc' = [pc EXCEPT ![self] = "put_parser"]
                      /\ UNCHANGED << fileElaborated, mainEnd, FileQueue, 
                                      PageQueue, WordQueue, file, item_, page, 
                                      i_, item, word, i >>

put_parser(self) == /\ pc[self] = "put_parser"
                    /\ IF i[self] > 0
                          THEN /\ WordQueue' = Append(WordQueue, word[self])
                               /\ i' = [i EXCEPT ![self] = i[self] - 1]
                               /\ pc' = [pc EXCEPT ![self] = "put_parser"]
                          ELSE /\ i' = [i EXCEPT ![self] = WordsPerPage]
                               /\ pc' = [pc EXCEPT ![self] = "while_loader"]
                               /\ UNCHANGED WordQueue
                    /\ UNCHANGED << fileElaborated, mainEnd, FileQueue, 
                                    PageQueue, file, item_, page, i_, item, 
                                    word >>

parser(self) == Parser(self) \/ while_loader(self) \/ parse_parser(self)
                   \/ put_parser(self)

(* Allow infinite stuttering to prevent deadlock on termination. *)
Terminating == /\ \A self \in ProcSet: pc[self] = "Done"
               /\ UNCHANGED vars

Next == main
           \/ (\E self \in {"Loader1", "Loader2"}: loader(self))
           \/ (\E self \in {"Parser1", "Parser2"}: parser(self))
           \/ Terminating

Spec == /\ Init /\ [][Next]_vars
        /\ WF_vars(main)
        /\ \A self \in {"Loader1", "Loader2"} : WF_vars(loader(self))
        /\ \A self \in {"Parser1", "Parser2"} : WF_vars(parser(self))

Termination == <>(\A self \in ProcSet: pc[self] = "Done")

\* END TRANSLATION 



=============================================================================
\* Modification History
\* Last modified Thu Apr 01 11:52:31 CEST 2021 by anitvam
\* Created Thu Apr 01 09:41:10 CEST 2021 by anitvam
