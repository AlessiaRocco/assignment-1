package main.utilities;

public class WordDetails {
    private final String word;
    private final Integer frequency;

    public WordDetails(String word, Integer frequency) {
        this.word = word;
        this.frequency = frequency;
    }

    public String getWord() {
        return word;
    }

    public Integer getFrequency() {
        return frequency;
    }

    @Override
    public String toString() {
        return word + " : " + frequency;
    }
}
