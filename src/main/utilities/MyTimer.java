package main.utilities;

public class MyTimer {

    private boolean running;
    private long startTime;

    public MyTimer(){
        running = false;
    }

    public void start(){
        running = true;
        startTime = System.currentTimeMillis();
    }

    public void stop(){
        startTime = getTime();
        running = false;
    }

    public long getTime(){
        if (running){
            return 	System.currentTimeMillis() - startTime;
        } else {
            return startTime;
        }
    }
}
