package main.utilities;

import java.io.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FileManager {

    public static List<String> createListOfPDF(File directory) {
        FilenameFilter pdfFilter = (dir, name) -> name.endsWith(".pdf");
        return Arrays.asList(directory.list(pdfFilter));
    }

    public static Set<String> extractWordsToBeIgnored(File fileName) {
        Set<String> stringsToBeAvoid = new HashSet<>();
        try {
            FileReader fr = new FileReader(fileName);   //reads the file
            BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream
            String line;
            while((line=br.readLine())!=null)
            {
                stringsToBeAvoid.add(line);
            }
            fr.close();    //closes the stream and release the resources
        } catch(IOException e) {
            e.printStackTrace();
        }
        return stringsToBeAvoid;
    }
}
