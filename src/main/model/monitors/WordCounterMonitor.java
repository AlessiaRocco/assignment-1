package main.model.monitors;

import main.utilities.ModelObserver;
import main.utilities.WordDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WordCounterMonitor {

    private final Map<String, Integer> wordMap;
    private final List<ModelObserver> observers;
    private int counter = 0;

    public WordCounterMonitor(){
        wordMap = new HashMap<>();
        observers = new ArrayList<>();
    }

    public void addObserver(ModelObserver obs){
        observers.add(obs);
    }

    private void notifyObservers(){
        for (ModelObserver obs: observers){
            obs.notifyWordUpdate(this);
        }
    }

    public synchronized void put(HashMap<String, Integer> word){
        word.forEach((w,f) -> {
            if (wordMap.containsKey(w)){
                wordMap.put(w, wordMap.get(w)+f);
            } else {
                wordMap.put(w, f);
            }
            counter+=f;
        });

        notifyObservers();
    }

    public synchronized int getNumOfParsedWords() {
        return counter;
    }

    public synchronized List<WordDetails> getMostFrequentsWords(int n){
        return wordMap.entrySet().stream()
                .sorted((value1, value2) -> value2.getValue().compareTo(value1.getValue()))
                .limit(n)
                .map(e -> new WordDetails(e.getKey(), e.getValue())).collect(Collectors.toList());

    }

    public synchronized Map<String, Integer> getWordMap(){
        return wordMap;
    }
}
