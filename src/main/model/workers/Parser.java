package main.model.workers;

import main.model.monitors.PageBufferMonitor;
import main.model.monitors.WordCounterMonitor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

public class Parser extends Worker {

    private final PageBufferMonitor pageMonitor;
    private final WordCounterMonitor wordCounterMonitor;
    private final Set<String> wordsToAvoid;

     public Parser(String name, PageBufferMonitor pageMonitor,
                   WordCounterMonitor wordCounterMonitor, Set<String> wordsToAvoid) {
        super(name);
        this.pageMonitor = pageMonitor;
        this.wordCounterMonitor = wordCounterMonitor;
        this.wordsToAvoid = wordsToAvoid;
    }

    @Override
    void work() {
        HashMap<String, Integer> words = new HashMap();
        String page = pageMonitor.get();
        if(page != null) {
            page = page.replaceAll("[,;:.!?()\"]", "");
            page = page.replaceAll("[^a-zA-Z0-9]", " ");
            page = page.toLowerCase();
            Arrays.asList(page.split(" ")).stream()
                    .filter(w -> !wordsToAvoid.contains(w) && !w.isEmpty())
                    .forEach(w -> {
                        if (words.containsKey(w)){
                            words.put(w, words.get(w)+1);
                        } else {
                            words.put(w, 1);
                        }
                    });
            wordCounterMonitor.put(words);
        }else{
            this.stopWorker();
        }
    }
}
