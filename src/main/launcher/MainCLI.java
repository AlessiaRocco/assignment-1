package main.launcher;


import main.model.monitors.FileBufferMonitor;
import main.model.monitors.PageBufferMonitor;
import main.model.monitors.WordCounterMonitor;
import main.model.workers.Loader;
import main.model.workers.Parser;
import main.utilities.MyTimer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static main.utilities.FileManager.createListOfPDF;
import static main.utilities.FileManager.extractWordsToBeIgnored;


public class MainCLI {

    public static void main(String[] args) {

        List<Loader> loaders = new ArrayList<>();
        List<Parser> parsers = new ArrayList<>();

        if (args.length == 0){
            System.err.println("Error! You have to specify 3 arguments: \n" +
                    "\t1. the folder where to retrieve the files, \n" +
                    "\t2. the number of the most frequent words you want to know, \n" +
                    "\t3. the file that contains all the words to avoid.");
            System.exit(-1);
        }
        String pathDir = args[0];
        List<String> pdfs = createListOfPDF(new File(pathDir));
        pdfs = pdfs.stream().map(s -> pathDir+"/"+s).collect(Collectors.toList());
        int pdfsSize = pdfs.size();
        int numberOfWords = Integer.valueOf(args[1]).intValue();
        Set<String> wordsIgnored = extractWordsToBeIgnored(new File(args[2]));

        WordCounterMonitor wordCounter = new WordCounterMonitor();
        PageBufferMonitor pageBuffer = new PageBufferMonitor(pdfsSize);
        FileBufferMonitor fileBuffer = new FileBufferMonitor(pdfs);

        int processors = Runtime.getRuntime().availableProcessors();
        int nLoader = processors < pdfsSize ? processors-1: pdfsSize;
        int nParser = processors-nLoader+2;

        for(int i = 0; i < nLoader; i++){
            loaders.add(new Loader("Loader" + i, fileBuffer, pageBuffer));
        }

        for(int i = 0; i < nParser; i++) {
            parsers.add(new Parser("Parser" + i, pageBuffer, wordCounter, wordsIgnored));
        }

        if (pdfs.isEmpty()){
            System.out.println("There aren't PDFs into the specified Directory");
            System.exit(0);
        }

        MyTimer timer = new MyTimer();

        timer.start();
        loaders.forEach(w -> w.start());
        parsers.forEach(w -> w.start());

        loaders.forEach(w -> {
            try {
                w.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        parsers.forEach(p -> {
            try {
                p.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        timer.stop();

        System.out.println("\n-----------------------------------------------------------------------------");
        System.out.println("Elaborated Words: " + wordCounter.getNumOfParsedWords());
        System.out.println("Most frequents words: ");
        wordCounter.getMostFrequentsWords(numberOfWords).forEach( w -> System.out.println(w));
        System.out.println("Time elapsed: " + timer.getTime() + "ms");
        System.out.println("-----------------------------------------------------------------------------");
    }
}
