package main.launcher;

import main.controller.Controller;
import main.view.Gui;

import javax.swing.*;

public class MainGUI {

    public static void main(String[] args) {
        Gui frame = new Gui(new Controller());
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }


}
