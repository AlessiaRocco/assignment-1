package main.view;

import main.controller.Controller;
import main.model.monitors.WordCounterMonitor;
import main.utilities.ModelObserver;
import main.utilities.javaSwing.SpringUtilities;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Gui extends JFrame implements ModelObserver {
    private final View view;
    private JPanel jContentPane = null;
    private JTextField elaboratedWords;
    private List<JLabel> mostFrequentWords;
    private List<JProgressBar> linesList;
    private JPanel resultsPanel;
    private JButton start;
    private JButton stop;
    private JTextField numFWordsTexTField;
    private JTextField pathDirTextField;
    private JTextField pathFileTextFiled;


    public Gui(Controller controller) {
        super();
        view = new View(controller);
        initialize();
    }

    private void initialize() {
        this.setSize(700, 500);
        this.setResizable(false);
        this.setLocation(this.getWidth()/2, this.getHeight()/3);
        this.setContentPane(getJContentPane());
        this.setTitle("Assignment #1");
    }

    private void setStartSettings(boolean isStart){
        this.start.setEnabled(isStart);
        this.stop.setEnabled(!isStart);
        pathDirTextField.setEditable(isStart);
        pathFileTextFiled.setEditable(isStart);
        numFWordsTexTField.setEditable(isStart);
        if(isStart) {
            this.showTime();
        }
    }

    private JPanel getJContentPane() {
        if (jContentPane == null) {
            jContentPane = new JPanel();
            jContentPane.setLayout(new BoxLayout(jContentPane, BoxLayout.Y_AXIS));

            /* INPUT PARAMETERS */
            JPanel jPanelLabels = new JPanel(new SpringLayout());
            jPanelLabels.setMaximumSize(new Dimension(this.getWidth(), this.getHeight()/2));

            JLabel label = new JLabel("Path Directory:", JLabel.TRAILING);
            pathDirTextField = new JTextField("input/", 10);
            jPanelLabels.add(label);
            jPanelLabels.add(pathDirTextField);

            label = new JLabel("Number of most frequent words:", JLabel.TRAILING);
            numFWordsTexTField = new JTextField("2", 10);
            jPanelLabels.add(label);
            jPanelLabels.add(numFWordsTexTField);

            label = new JLabel("Path File:", JLabel.TRAILING);
            pathFileTextFiled = new JTextField("input/avoid.txt", 10);
            jPanelLabels.add(label);
            jPanelLabels.add(pathFileTextFiled);

            //Lay out the panel for labels.
            SpringUtilities.makeCompactGrid(jPanelLabels,
                    3, 2,           //rows, cols
                    10, 10,        //initX, initY
                    15, 15);       //xPad, yPad

            /*--------------------------------------------------------------------------------------------------------*/
            /* BUTTONS TO CONTROL THE EXECUTION */
            stop = new JButton("Stop");
            JPanel jPanelButtons = new JPanel(new FlowLayout());
            start = new JButton("Start");
            start.addActionListener(actionEvent -> {
                String args1 = pathDirTextField.getText();
                String args3 = pathFileTextFiled.getText();
                setStartSettings(false);
                resultsPanel.removeAll();
                resultsPanel.add(getPanelResults());
                view.eventStart(args1, args3, Gui.this);
            });

            stop.addActionListener(actionEvent -> {
                setStartSettings(true);
            });

            stop.setEnabled(false);

            jPanelButtons.add(start);
            jPanelButtons.add(stop);
            /*--------------------------------------------------------------------------------------------------------*/
            /* GENERAL EXECUTION INFORMATION */
            JPanel jPanelExecution = new JPanel(new SpringLayout());
            jPanelExecution.setMaximumSize(new Dimension(this.getWidth(), 100));

            label = new JLabel("Elaborated words:", JLabel.TRAILING);
            elaboratedWords = new JTextField("0", 10);
            elaboratedWords.setEditable(false);
            jPanelExecution.add(label);
            jPanelExecution.add(elaboratedWords);

            label = new JLabel("Most frequent words:", JLabel.TRAILING);
            jPanelExecution.add(label);
            jPanelExecution.add(new JLabel(""));
            SpringUtilities.makeCompactGrid(jPanelExecution,
                    2, 2,           //rows, cols
                    10, 10,        //initX, initY
                    1, 1);       //xPad, yPad
            /*--------------------------------------------------------------------------------------------------------*/

            jContentPane.add(jPanelLabels);
            jContentPane.add(jPanelButtons);
            jContentPane.add(jPanelExecution);
            resultsPanel = new JPanel();

            JScrollPane jScrollPane = new JScrollPane(resultsPanel);
            jScrollPane.setAutoscrolls(false);
            jScrollPane.setMaximumSize(new Dimension(Gui.this.getWidth(), 2000000));
            jContentPane.add(jScrollPane);

        }
        return jContentPane;
    }

    @Override
    public void notifyWordUpdate(WordCounterMonitor model) {
        SwingUtilities.invokeLater(()->{
            int parsedWords = model.getNumOfParsedWords();

            elaboratedWords.setText(String.valueOf(parsedWords));
            List<Map.Entry<String, Integer>> words = view.checkWords(model, Integer
                    .parseInt(numFWordsTexTField.getText()));
            mostFrequentWords.forEach(a -> a.setText(""));
            for(int i = 0; i < words.size(); i++){
                mostFrequentWords.get(i).setText(words.get(i).getKey());
                Dimension d = linesList.get(i).getSize();
                linesList.get(i).setMaximum(parsedWords);
                linesList.get( i ).setValue(words.get(i).getValue());
            }
        });
    }

    @Override
    public void notifyEnd() {
        setStartSettings(true);
    }

    private void showTime() {
        JOptionPane.showMessageDialog(this,
                "Time Elapsed: " + view.eventStop() + " ms");
    }

    private JPanel getPanelResults(){
        JPanel container = new JPanel();
        container.setLayout(new GridLayout(0,1));
        container.setPreferredSize(new Dimension(600, 50*Integer.parseInt(numFWordsTexTField.getText())));
        linesList = new ArrayList<>();

        mostFrequentWords = new ArrayList<>();
        for(int i = 0; i < Integer.parseInt(numFWordsTexTField.getText()); i++){
            mostFrequentWords.add(createMyLabel());
            container.add(mostFrequentWords.get(i));

            JProgressBar progressBar = new JProgressBar();
            linesList.add(progressBar);
            container.add(linesList.get(i));
        }
        return container;
    }

    private JLabel createMyLabel(){
        JLabel label = new JLabel();
        label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
        label.setBorder(new EmptyBorder(10, 0, 0, 0));
        label.setHorizontalAlignment(JLabel.LEFT);
        label.setPreferredSize(new Dimension(this.getWidth()-50, 40));
        return label;
    }
}
