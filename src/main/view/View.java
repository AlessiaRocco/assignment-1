package main.view;

import main.controller.Controller;
import main.model.monitors.WordCounterMonitor;
import main.utilities.ModelObserver;
import main.utilities.MyTimer;
import main.utilities.WordDetails;

import javax.swing.*;
import java.util.*;

public class View extends JFrame {

    private Controller controller;
    private List<WordDetails> words;

    private final MyTimer timer;

    public View(Controller controller){
        this.controller = controller;
        timer = new MyTimer();
    }

    public void eventStart(String directory, String avoidFile, ModelObserver observer){
        controller.startComputation(directory, avoidFile, observer);
        timer.start();
    }

    public List<Map.Entry<String, Integer>> checkWords(WordCounterMonitor model, int numberOfWords){
        words = model.getMostFrequentsWords(numberOfWords);
        List<Map.Entry<String,Integer>> output = new ArrayList<>();

        for (int i = 0; i < words.size(); i++){
            Map.Entry<String, Integer> entry = new AbstractMap.SimpleEntry<String, Integer>(i+1 + ". " + words.get(i) + " times.", words.get(i).getFrequency());
            output.add(entry);
        }
        return output;
    }

    public long eventStop(){
        controller.stopComputation();
        timer.stop();
        return timer.getTime();
    }

}

