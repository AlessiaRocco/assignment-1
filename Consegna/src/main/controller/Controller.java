package main.controller;

import main.model.monitors.FileBufferMonitor;
import main.model.monitors.PageBufferMonitor;
import main.model.monitors.WordCounterMonitor;
import main.model.workers.Loader;
import main.model.workers.Parser;
import main.model.workers.Worker;
import main.utilities.ModelObserver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static main.utilities.FileManager.createListOfPDF;
import static main.utilities.FileManager.extractWordsToBeIgnored;


public class Controller {

    private List<Worker> workers = new ArrayList<>();

    public void startComputation(String pathDir, String pathAvoidedWordsFile, ModelObserver observer){
        new Thread(() -> {
            List<String> pdfs = createListOfPDF(new File(pathDir));
            pdfs = pdfs.stream().map(s -> pathDir+"/"+s).collect(Collectors.toList());
            int pdfsSize = pdfs.size();
            Set<String> wordsIgnored = extractWordsToBeIgnored(new File(pathAvoidedWordsFile));

            WordCounterMonitor counterMonitor =  new WordCounterMonitor();
            counterMonitor.addObserver(observer);
            PageBufferMonitor pageBuffer = new PageBufferMonitor(pdfsSize);
            pageBuffer.addObserver(observer);
            FileBufferMonitor fileBuffer = new FileBufferMonitor(pdfs);

            int processors = Runtime.getRuntime().availableProcessors();
            int nLoader = processors < pdfsSize ? processors-3: pdfsSize;
            int nParser = processors-nLoader;
            for(int i = 0; i < nLoader; i++){
                workers.add(new Loader("Loader" + i, fileBuffer, pageBuffer));
            }

            for(int i = 0; i < nParser; i++) {
                workers.add(new Parser("Parser" + i, pageBuffer, counterMonitor, wordsIgnored));
            }

            workers.forEach(w -> w.start());
        }).start();
    }
    public void stopComputation(){
            workers.forEach(w -> w.stopWorker());
            workers = new ArrayList<>();
    }


}
