package main.utilities;

import main.model.monitors.WordCounterMonitor;

public interface ModelObserver {
    void notifyWordUpdate(WordCounterMonitor model);
    void notifyEnd();
}
