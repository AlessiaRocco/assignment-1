package main.model.workers;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.text.PDFTextStripper;
import main.model.monitors.FileBufferMonitor;
import main.model.monitors.PageBufferMonitor;

import java.io.File;
import java.io.IOException;

public class Loader extends Worker {

    FileBufferMonitor fileBuffer;
    PageBufferMonitor pageBuffer;


    public Loader(final String name, final FileBufferMonitor fileMonitor, final PageBufferMonitor pageMonitor) {
        super(name);
        this.fileBuffer = fileMonitor;
        this.pageBuffer = pageMonitor;
    }

    @Override
    void work() {
        String fileName = fileBuffer.get();

        if (fileName != null){
            try {
                extractStringFromPDF(fileName);
            } catch (IOException e) {
                System.err.println("Error: Cannot parse" + fileName);
                System.exit(-1);
            }

        } else {
            this.stopWorker();
        }

    }

    private void extractStringFromPDF (final String fileName) throws IOException {
        PDDocument document = PDDocument.load(new File(fileName));
        AccessPermission ap = document.getCurrentAccessPermission();
        if (!ap.canExtractContent()) {
            throw new IOException();
        }

        PDFTextStripper stripper = new PDFTextStripper();
        stripper.setSortByPosition(true);

        for (int p = 1; p <= document.getNumberOfPages(); ++p)
        {
            stripper.setStartPage(p);
            stripper.setEndPage(p);
            String text = stripper.getText(document);

            pageBuffer.set(text.trim(), p == document.getNumberOfPages());
        }
        document.close();
    }
}
