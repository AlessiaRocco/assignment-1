package main.model.workers;

public abstract class Worker extends Thread {

    private boolean stop = false;

    protected Worker(String name) {
        super(name);
    }


    abstract void work();

    @Override
    public void run() {
        while (!stop){
                work();
            try {
                sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void stopWorker(){
        log("Stopped");
        this.stop = true;
    }
    
    protected void log(String st){
        synchronized(System.out){
            System.out.println("["+this.getName()+"] "+st);
        }
    }


}
