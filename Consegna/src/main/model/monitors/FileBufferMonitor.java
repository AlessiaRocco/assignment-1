package main.model.monitors;

import java.util.LinkedList;
import java.util.List;

public class FileBufferMonitor {
    private LinkedList<String> fileQueue;

    public FileBufferMonitor(final List<String> queue){
        this.fileQueue = new LinkedList<>(queue);
    }

    public synchronized String get() {
        return fileQueue.poll();
    }
}
