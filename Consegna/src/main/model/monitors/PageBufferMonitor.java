package main.model.monitors;

import main.utilities.ModelObserver;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PageBufferMonitor {

    private final LinkedList<String> pages;
    private final List<ModelObserver> observers;
    private boolean alreadyNotify = false;
    private int numberOfPDF;

    public PageBufferMonitor(int numberOfPDF){
        pages = new LinkedList<>();
        observers = new ArrayList<>();
        this.numberOfPDF = numberOfPDF;
    }

    public void addObserver(ModelObserver obs){
        observers.add(obs);
    }

    private void notifyObservers(){
        for (ModelObserver obs: observers){
            obs.notifyEnd();
        }
    }

    public synchronized void set(String page, Boolean isLastPage){
        this.pages.add(page);
        if (isLastPage) {
            numberOfPDF--;
        }
        notifyAll();
    }

    public synchronized String get() {

        while (pages.isEmpty() && numberOfPDF > 0){
            try {
                wait();
            } catch (InterruptedException ex){}
        }
        String page = pages.poll();
        if(page == null && !alreadyNotify){
            alreadyNotify = true;
            notifyObservers();
        }
        return page;
    }

}
